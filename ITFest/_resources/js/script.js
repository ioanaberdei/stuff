/*Acest fisier va contine javascript-urile ce sunt folosite pe toate paginile*/


(function($){
    $(function(){

        $('.button-collapse').sideNav();
        $('.parallax').parallax();

    }); // end of document ready
})(jQuery); // end of jQuery name space

$(document).ready(function(){
	$(".dropdown-button").dropdown();
})

/* animatie navbar */

  $(document).ready(function(){
      var scroll_start = 0;
      var startchange = $('#startchange');
      var offset = startchange.offset();
       if (startchange.length){
      $(document).scroll(function() {
         scroll_start = $(this).scrollTop();
         if(scroll_start > offset.top) {
           $( 'nav' ).animate({
  opacity: 1.0,
}, 1000, function() {
  // Animation complete.
});
          }
      });
       }
   });